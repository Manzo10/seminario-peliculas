package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.ProduccionInterface;
import com.uniajc.peliculasbackend.interfaces.RolInterface;
import com.uniajc.peliculasbackend.modelos.Produccion;
import com.uniajc.peliculasbackend.modelos.Rol;
import com.uniajc.peliculasbackend.repositorios.ProduccionRepositorio;
import com.uniajc.peliculasbackend.repositorios.RolRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RolServicio implements RolInterface {
    @Autowired
    RolRepositorio rolRepositorio;

    @Override
    public void crearRol (Rol rol) {
        if (rol != null) {
            rolRepositorio.save(rol);
        }
    }

    @Override
    public Rol obtenerRolId(int id) {

        return rolRepositorio.obtenerRolId(id);
    }

    @Override
    public List<Rol> obtenerTodosRol() {
        return rolRepositorio.findAll();
    }
}
