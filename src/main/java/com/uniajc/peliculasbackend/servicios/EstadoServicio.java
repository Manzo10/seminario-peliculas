package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.EstadoInterface;
import com.uniajc.peliculasbackend.modelos.Estado;
import com.uniajc.peliculasbackend.repositorios.EstadoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoServicio implements EstadoInterface {
    @Autowired
    EstadoRepositorio estadoRepositorio;

    @Override
    public void crearEstado(Estado estado) {
        if (estado != null) {
            estadoRepositorio.save(estado);
        }
    }

    @Override
    public Estado obtenerEstadoId(int id) {

        return estadoRepositorio.obtenerEstadoId(id);
    }

    @Override
    public List<Estado> obtenerTodosEstados() {
        return estadoRepositorio.findAll();
    }
}
