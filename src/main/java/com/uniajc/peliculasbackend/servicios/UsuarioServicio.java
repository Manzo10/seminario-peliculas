package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.RolInterface;
import com.uniajc.peliculasbackend.interfaces.UsuarioInterface;
import com.uniajc.peliculasbackend.modelos.Rol;
import com.uniajc.peliculasbackend.modelos.Usuario;
import com.uniajc.peliculasbackend.repositorios.RolRepositorio;
import com.uniajc.peliculasbackend.repositorios.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UsuarioServicio implements UsuarioInterface {
    @Autowired
    UsuarioRepositorio usuarioRepositorio;

    @Override
    public void crearUsuario (Usuario usuario) {
        if (usuario != null) {
            usuarioRepositorio.save(usuario);
        }
    }

    @Override
    public Usuario obtenerUsuarioId(int id) {

        return usuarioRepositorio.obtenerUsuarioId(id);
    }

    @Override
    public List<Usuario> obtenerTodosUsuario() {
        return usuarioRepositorio.findAll();
    }
}
