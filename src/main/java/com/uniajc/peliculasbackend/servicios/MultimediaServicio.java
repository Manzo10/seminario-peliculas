package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.MultimediaInterface;
import com.uniajc.peliculasbackend.modelos.Multimedia;
import com.uniajc.peliculasbackend.repositorios.MultimediaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MultimediaServicio implements MultimediaInterface {
    @Autowired
    MultimediaRepositorio multimediaRepositorio;

    @Override
    public void crearMultimedia(Multimedia multimedia) {
        if (multimedia != null) {
            multimediaRepositorio.save(multimedia);
        }
    }

    @Override
    public Multimedia obtenerMultimediaId(int id) {

        return multimediaRepositorio.obtenerMultimediaId(id);
    }

    @Override
    public List<Multimedia> obtenerTodosMultimedia() {
        return multimediaRepositorio.findAll();
    }
}
