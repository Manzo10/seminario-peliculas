package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.Generointerface;
import com.uniajc.peliculasbackend.modelos.Genero;
import com.uniajc.peliculasbackend.repositorios.GeneroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class GeneroServicio implements Generointerface {
    @Autowired
    GeneroRepositorio generoRepositorio;

    @Override
    public void crearGenero(Genero genero) {
        if (genero != null) {
            generoRepositorio.save(genero);
        }
    }

    @Override
    public Genero obtenerGeneroId(int id) {

        return generoRepositorio.obtenerGeneroId(id);
    }

    @Override
    public List<Genero> obtenerTodosGenero() {
        return generoRepositorio.findAll();
    }
}
