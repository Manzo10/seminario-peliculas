package com.uniajc.peliculasbackend.servicios;

import com.uniajc.peliculasbackend.interfaces.MultimediaInterface;
import com.uniajc.peliculasbackend.interfaces.ProduccionInterface;
import com.uniajc.peliculasbackend.modelos.Multimedia;
import com.uniajc.peliculasbackend.modelos.Produccion;
import com.uniajc.peliculasbackend.repositorios.MultimediaRepositorio;
import com.uniajc.peliculasbackend.repositorios.ProduccionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProduccionServicio implements ProduccionInterface {
    @Autowired
    ProduccionRepositorio produccionRepositorio;

    @Override
    public void crearProduccion (Produccion produccion) {
        if (produccion != null) {
            produccionRepositorio.save(produccion);
        }
    }

    @Override
    public Produccion obtenerProduccionId(int id) {

        return produccionRepositorio.obtenerProduccionId(id);
    }

    @Override
    public List<Produccion> obtenerTodosProduccion() {
        return produccionRepositorio.findAll();
    }
}
