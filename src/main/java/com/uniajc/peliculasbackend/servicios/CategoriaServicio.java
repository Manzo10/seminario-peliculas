package com.uniajc.peliculasbackend.servicios;
import com.uniajc.peliculasbackend.interfaces.Categoriainterface;
import com.uniajc.peliculasbackend.modelos.Categoria;
import com.uniajc.peliculasbackend.repositorios.CategoriaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoriaServicio implements Categoriainterface {
    @Autowired
   CategoriaRepositorio categoriaRepositorio;

    @Override
    public void crearCategoria(Categoria categoria) {
        if (categoria != null) {
            categoriaRepositorio.save(categoria);
        }
    }

    @Override
    public Categoria obtenerCategoriaId(int id) {
        {
            return categoriaRepositorio.obtenerCategoriaId(id);
        }
    }
        @Override
        public List<Categoria> obtenerTodosCategoria() {
            return categoriaRepositorio.findAll();


    }
}
