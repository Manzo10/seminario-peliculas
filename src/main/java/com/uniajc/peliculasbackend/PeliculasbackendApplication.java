package com.uniajc.peliculasbackend;

import com.uniajc.peliculasbackend.modelos.Estado;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PeliculasbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeliculasbackendApplication.class, args);
	}

}
