package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Estado;
import com.uniajc.peliculasbackend.modelos.Genero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GeneroRepositorio extends JpaRepository<Genero, Integer> {
    @Query(value = "FROM Genero g WHERE g.id = :id")
    Genero obtenerGeneroId(@Param(value = "id") int id);

}
