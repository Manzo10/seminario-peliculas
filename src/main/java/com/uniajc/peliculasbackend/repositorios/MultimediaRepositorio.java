package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Genero;
import com.uniajc.peliculasbackend.modelos.Multimedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MultimediaRepositorio extends JpaRepository<Multimedia, Integer> {
    @Query(value = "FROM Multimedia m WHERE m.id = :id")
    Multimedia obtenerMultimediaId(@Param(value = "id") int id);

}
