package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Categoria;
import com.uniajc.peliculasbackend.modelos.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoriaRepositorio extends JpaRepository<Categoria, Integer> {
    @Query(value = "FROM Categoria c WHERE c.id = :id")
    Categoria obtenerCategoriaId(@Param(value = "id") int id);
}
