package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Multimedia;
import com.uniajc.peliculasbackend.modelos.Produccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProduccionRepositorio extends JpaRepository<Produccion, Integer> {
    @Query(value = "FROM Produccion p WHERE p.id = :id")
    Produccion obtenerProduccionId(@Param(value = "id") int id);

}
