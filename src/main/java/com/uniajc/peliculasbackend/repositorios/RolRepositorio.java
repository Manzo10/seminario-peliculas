package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Produccion;
import com.uniajc.peliculasbackend.modelos.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RolRepositorio extends JpaRepository<Rol, Integer> {
    @Query(value = "FROM Rol r WHERE r.id = :id")
    Rol obtenerRolId(@Param(value = "id") int id);

}
