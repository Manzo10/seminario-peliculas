package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Rol;
import com.uniajc.peliculasbackend.modelos.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsuarioRepositorio extends JpaRepository<Usuario, Integer> {
    @Query(value = "FROM Usuario u WHERE u.id = :id")
    Usuario obtenerUsuarioId(@Param(value = "id") int id);

}
