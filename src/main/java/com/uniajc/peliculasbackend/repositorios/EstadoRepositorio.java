package com.uniajc.peliculasbackend.repositorios;

import com.uniajc.peliculasbackend.modelos.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EstadoRepositorio extends JpaRepository<Estado, Integer> {
    @Query(value = "FROM Estado e WHERE e.id = :id")
    Estado obtenerEstadoId(@Param(value = "id") int id);



}
