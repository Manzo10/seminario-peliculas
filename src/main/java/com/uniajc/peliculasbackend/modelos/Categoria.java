package com.uniajc.peliculasbackend.modelos;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "categoria")
public class Categoria {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "descripcion")
    private String descripcion;
    @Basic
    @Column(name = "id_estado")
    private int idEstado;

}
