package com.uniajc.peliculasbackend.modelos;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "produccion")
public class Produccion {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column(name = "sipnosis")
    private String sipnosis;
    @Basic
    @Column(name = "director")
    private String director;
    @Basic
    @Column(name = "actores")
    private String actores;
    @Basic
    @Column(name = "imagen_preview")
    private String imagenPreview;
    @Basic
    @Column(name = "id_categoria")
    private int idCategoria;
    @Basic
    @Column(name = "id_genero")
    private int idGenero;
    @Basic
    @Column(name = "id_estado")
    private int idEstado;

}
