package com.uniajc.peliculasbackend.modelos;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "multimedia")
public class Multimedia {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "id_produccion")
    private int idProduccion;
    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column(name = "descripcion")
    private String descripcion;
    @Basic
    @Column(name = "archivo_multimedia")
    private String archivoMultimedia;
    @Basic
    @Column(name = "id_estado")
    private int idEstado;

}
