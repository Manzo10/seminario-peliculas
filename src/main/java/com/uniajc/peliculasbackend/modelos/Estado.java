package com.uniajc.peliculasbackend.modelos;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "estado")
public class Estado {

    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "descripcion")
    private String descripcion;

}


