package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.ProduccionInterface;
import com.uniajc.peliculasbackend.interfaces.RolInterface;
import com.uniajc.peliculasbackend.modelos.Produccion;
import com.uniajc.peliculasbackend.modelos.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/rol")
public class RolControlador {
    @Autowired
    RolInterface rolinterface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearRol(@RequestBody Rol rol) {
        rolinterface.crearRol(rol);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Rol> obtenerTodosRol() {
        return rolinterface.obtenerTodosRol();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Rol obtenerRolId(@RequestParam(name = "id") int id) {
        return rolinterface.obtenerRolId(id);
    }
}
