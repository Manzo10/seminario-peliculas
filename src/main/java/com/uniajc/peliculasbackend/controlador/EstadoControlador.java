package com.uniajc.peliculasbackend.controlador;

import com.uniajc.peliculasbackend.interfaces.EstadoInterface;
import com.uniajc.peliculasbackend.modelos.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/estado")
public class EstadoControlador {

    @Autowired
    EstadoInterface estadoInterface;


    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearEstado(@RequestBody Estado estado) {
        estadoInterface.crearEstado(estado);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Estado> obtenerTodosEstados() {
        return estadoInterface.obtenerTodosEstados();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Estado obtenerEstadoId(@RequestParam(name = "id") int id) {
        return estadoInterface.obtenerEstadoId(id);
    }
}
