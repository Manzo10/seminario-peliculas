package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.RolInterface;
import com.uniajc.peliculasbackend.interfaces.UsuarioInterface;
import com.uniajc.peliculasbackend.modelos.Rol;
import com.uniajc.peliculasbackend.modelos.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/usuario")
public class UsuarioControlador {
    @Autowired
    UsuarioInterface usuariointerface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearUsuario(@RequestBody Usuario usuario) {
        usuariointerface.crearUsuario(usuario);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Usuario> obtenerTodosUsuario() {
        return usuariointerface.obtenerTodosUsuario();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Usuario obtenerUsuarioId(@RequestParam(name = "id") int id) {
        return usuariointerface.obtenerUsuarioId(id);
    }
}
