package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.Categoriainterface;
import com.uniajc.peliculasbackend.modelos.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/categoria")
public class CategoriaControlador {
    @Autowired
    Categoriainterface categoriainterface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearCategoria(Categoria estado) {
        categoriainterface.crearCategoria(estado);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Categoria> obtenerTodosCategoria() {
        return categoriainterface.obtenerTodosCategoria();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Categoria obtenerCategoriaId(@RequestParam(name = "id") int id) {
        return categoriainterface.obtenerCategoriaId(id);
    }
}
