package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.MultimediaInterface;
import com.uniajc.peliculasbackend.interfaces.ProduccionInterface;
import com.uniajc.peliculasbackend.modelos.Multimedia;
import com.uniajc.peliculasbackend.modelos.Produccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/produccion")
public class ProduccionControlador {
    @Autowired
    ProduccionInterface produccioninterface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearProduccion(@RequestBody Produccion produccion) {
        produccioninterface.crearProduccion(produccion);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Produccion> obtenerTodosProduccion() {
        return produccioninterface.obtenerTodosProduccion();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Produccion obtenerProduccionId(@RequestParam(name = "id") int id) {
        return produccioninterface.obtenerProduccionId(id);
    }
}
