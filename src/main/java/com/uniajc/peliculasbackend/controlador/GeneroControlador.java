package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.Generointerface;
import com.uniajc.peliculasbackend.modelos.Genero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/genero")
public class GeneroControlador {
    @Autowired
    Generointerface generointerface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearGenero(@RequestBody Genero genero) {
        generointerface.crearGenero(genero);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Genero> obtenerTodosGenero() {
        return generointerface.obtenerTodosGenero();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Genero obtenerGeneroId(@RequestParam(name = "id") int id) {
        return generointerface.obtenerGeneroId(id);
    }
}
