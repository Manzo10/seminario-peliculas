package com.uniajc.peliculasbackend.controlador;


import com.uniajc.peliculasbackend.interfaces.Generointerface;
import com.uniajc.peliculasbackend.interfaces.MultimediaInterface;
import com.uniajc.peliculasbackend.modelos.Genero;
import com.uniajc.peliculasbackend.modelos.Multimedia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/multimedia")
public class MultimediaControlador {
    @Autowired
    MultimediaInterface multimediainterface;

    @RequestMapping(path = "/crear", method = RequestMethod.POST)
    public void crearMultimedia(@RequestBody Multimedia multimedia) {
        multimediainterface.crearMultimedia(multimedia);
    }

    @RequestMapping(path = "/todo", method = RequestMethod.GET)
    public List<Multimedia> obtenerTodosMultimedia() {
        return multimediainterface.obtenerTodosMultimedia();
    }

    @RequestMapping(path = "/id", method = RequestMethod.GET)
    public Multimedia obtenerGeneroId(@RequestParam(name = "id") int id) {
        return multimediainterface.obtenerMultimediaId(id);
    }
}
