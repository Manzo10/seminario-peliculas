package com.uniajc.peliculasbackend.interfaces;

import com.uniajc.peliculasbackend.modelos.Estado;

import java.util.List;

public interface EstadoInterface {
    void crearEstado(Estado estado);
    Estado obtenerEstadoId(int id);
    List<Estado> obtenerTodosEstados();
}
