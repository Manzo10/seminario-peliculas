package com.uniajc.peliculasbackend.interfaces;


import com.uniajc.peliculasbackend.modelos.Produccion;
import com.uniajc.peliculasbackend.modelos.Rol;

import java.util.List;

public interface RolInterface {
    void crearRol(Rol rol);
    Rol obtenerRolId(int id);
    List<Rol> obtenerTodosRol();
}
