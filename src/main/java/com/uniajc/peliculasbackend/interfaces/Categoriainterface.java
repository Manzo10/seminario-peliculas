package com.uniajc.peliculasbackend.interfaces;

import com.uniajc.peliculasbackend.modelos.Categoria;

import java.util.List;

public interface Categoriainterface {
    void crearCategoria(Categoria categoria);
    Categoria obtenerCategoriaId(int id);
    List<Categoria> obtenerTodosCategoria();
}
