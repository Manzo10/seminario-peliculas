package com.uniajc.peliculasbackend.interfaces;


import com.uniajc.peliculasbackend.modelos.Multimedia;
import com.uniajc.peliculasbackend.modelos.Produccion;

import java.util.List;

public interface ProduccionInterface {
    void crearProduccion(Produccion produccion);
    Produccion obtenerProduccionId(int id);
    List<Produccion> obtenerTodosProduccion();
}
