package com.uniajc.peliculasbackend.interfaces;


import com.uniajc.peliculasbackend.modelos.Rol;
import com.uniajc.peliculasbackend.modelos.Usuario;

import java.util.List;

public interface UsuarioInterface {
    void crearUsuario(Usuario usuario);
    Usuario obtenerUsuarioId(int id);
    List<Usuario> obtenerTodosUsuario();
}
