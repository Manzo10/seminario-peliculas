package com.uniajc.peliculasbackend.interfaces;


import com.uniajc.peliculasbackend.modelos.Multimedia;


import java.util.List;

public interface MultimediaInterface {
    void crearMultimedia(Multimedia multimedia);
    Multimedia obtenerMultimediaId(int id);
    List<Multimedia> obtenerTodosMultimedia();
}
