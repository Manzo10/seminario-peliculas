package com.uniajc.peliculasbackend.interfaces;

import com.uniajc.peliculasbackend.modelos.Categoria;
import com.uniajc.peliculasbackend.modelos.Genero;

import java.util.List;

public interface Generointerface {
    void crearGenero(Genero genero);
    Genero obtenerGeneroId(int id);
    List<Genero> obtenerTodosGenero();
}
